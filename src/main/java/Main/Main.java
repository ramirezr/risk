package Main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.internal.build.AllowSysOut;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.sql.ast.Clause;

import DAO.ContinentDAOImpl;
import DAO.IContinentDAO;
import DAO.IJugadorDAO;
import DAO.IRegioDAO;
import DAO.JugadorDAOImpl;
import DAO.RegioDAOImpl;
import Model.Continent;
import Model.Jugador;
import Model.Regio;

public class Main {
	public static LeerScanner reader;
	static boolean bbddCargada = false;

	public static void main(String[] args) {
		reader = new LeerScanner();
		boolean salir = false;
		int opcion;
		int numJugadors;
		JugadorDAOImpl jDao = new JugadorDAOImpl();
		ContinentDAOImpl cDao = new ContinentDAOImpl();
		RegioDAOImpl rDao = new RegioDAOImpl();
		
		while (!salir) {
			EscribirMenu();
			opcion = Integer.parseInt(reader.LlegirPantalla());

			switch (opcion) {
			case 0:
				salir = true;
				break;
			case 1:
				numJugadors=0;
				if(!bbddCargada) {
					System.out.println("Quants jugadors jugaran el Joc? ");
					System.out.println("Entre 2 i 4 jugadors:");
					while(numJugadors <2 || numJugadors>4) {
						numJugadors = Integer.parseInt(reader.LlegirPantalla());
						if(numJugadors <2 || numJugadors>4) {
							System.out.println("El numero "+numJugadors+" ha de ser entre 2 i 4!");
						}
					}
					cargarTablero(jDao, cDao, rDao, numJugadors);
					System.out.println("El tablero se ha cargado en base de datos.");
				} else {
					System.out.println("La base de dades ja ha sigut carregada anteriorment! \nSi vols tornar a jugar surt del programa i torna a entrar!");
				}
				
				
				break;
			case 2:
				int i = 0;
				boolean atacar = false;
				boolean reestructuar;
				int CodiJugador=0;
				
				Random random = new Random();
				if(bbddCargada) {
					System.out.println("Partida comenzada!");
					List<Jugador> jugadors = jDao.findAll();
					Collections.sort(jugadors);
					do{
						if(i < jugadors.size()) {
							Jugador j = jugadors.get(i);						
							rDao.PosarTropes(j.getId(), jDao.CalcularTropes(j.getId()));
							atacar = true;
							rDao.FaseAtac(j.getId(), atacar);
							reestructuar = random.nextBoolean();
							if(reestructuar == true) {
								rDao.FaseReestructuracio(j.getId());
							}
							cDao.CanviarPropietariContinent();
							i++;
						}
						else i = 0;
						
					}while(!jDao.FinalitzarPartida());
					System.out.println("Partida acabada!");
				}
				else {
					System.err.println("Tienes que llenar el tablero antes de jugar!");
				}
				break;
			case 3:
					System.out.println("Posa el codi del jugador per veure les seves regions: ");
					CodiJugador= Integer.parseInt(reader.LlegirPantalla());
					List <Regio> regiones = rDao.getRegionsJugador(CodiJugador);
					if(regiones.isEmpty()) {
						System.out.println("No te regions");

					}else {
						System.out.println("Regions del jugador" + CodiJugador + ":\n" + regiones);
					}
					
				break;
			case 4:
				System.out.println("introduce un id del jugador para ponerle las tropas.");
				CodiJugador = Integer.parseInt(reader.LlegirPantalla());
				rDao.PosarTropes(CodiJugador, jDao.CalcularTropes(CodiJugador));
				break;
			case 5:
				System.out.println("Aqui es mostren els continents que han sigut canviats: ");
				cDao.CanviarPropietariContinent();
				break;
			default:
				System.out.println("Opcion Incorrecta!!");
			}
		}

	}

	private static void EscribirMenu() {
		System.out.println("-------------------------RISK-------------------------");
		System.out.println("Selecciona una opcion:");
		// Propietats
		System.out.println("0. Salir");
		System.out.println("1. Cargar tablero.");
		System.out.println("2. Simular partida.");
		System.out.println("3. LlistarRegionsJugador(int CodiJugador)");
		System.out.println("4. PosarTropes(int CodiJugador)");
		System.out.println("5. CanviarPropietariContinent()");
		
	}

	private static void cargarTablero(IJugadorDAO jdao, IContinentDAO cdao, IRegioDAO rdao, int numJugadors) {
		List<Continent> continents = new ArrayList<Continent>();
		List<Regio> rrr ;
		//si només tenim 2 jugador
		if(numJugadors==2) {
			
			Jugador adrian = new Jugador("Adrian", 0);
			Jugador amina = new Jugador("Amina", 1);

			jdao.saveOrUpdate(adrian);
			jdao.saveOrUpdate(amina);
			
			//creem el continent africa i les seves regions
			Continent africa = new Continent("Africa");
			cdao.saveOrUpdate(africa);
			Regio marruecos = new Regio("Marruecos", africa, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(marruecos);
			Regio congo = new Regio("Congo", africa, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(congo);
			Regio algeria = new Regio("Algeria", africa, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(algeria);
			Regio mali = new Regio("Mali", africa, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(mali);
			Regio libia = new Regio("Libia", africa, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(libia);
			rrr = rdao.findAll();
			for(Regio r : rrr) {
				if(r.getContinent().equals(africa)) {
					africa.getRegions().add(r);
				}
			}
			for (Regio r : africa.getRegions()) {
				r = setVeins(africa, r);
			}
			continents.add(africa);

			//creem el continent africa
			Continent europa = new Continent("Europa");
			cdao.saveOrUpdate(europa);
			Regio espanya = new Regio("Espanya", europa, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(espanya);
			Regio francia = new Regio("Francia", europa, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(francia);
			Regio alemania = new Regio("Alemania", europa, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(alemania);
			Regio italia = new Regio("Italia", europa, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(italia);
			Regio austria = new Regio("Austria", europa, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(austria);
			rrr = rdao.findAll();
			for(Regio r : rrr) {
				if(r.getContinent().equals(europa)) {
					europa.getRegions().add(r);
				}
			}
			for (Regio r : europa.getRegions()) {
				r = setVeins(europa, r);
			}
			
			continents.add(europa);
			espanya.getVeins().add(marruecos);
			marruecos.getVeins().add(alemania);
			austria.getVeins().add(congo);
			
			rdao.saveOrUpdate(austria);
			rdao.saveOrUpdate(alemania);
			rdao.saveOrUpdate(congo);
			rdao.saveOrUpdate(espanya);
			rdao.saveOrUpdate(marruecos);		
			jdao.saveOrUpdate(amina);
			jdao.saveOrUpdate(adrian);
		}

		else if(numJugadors==3) {
			Jugador adrian = new Jugador("Adrian", 0);
			Jugador amina = new Jugador("Amina", 1);
			Jugador raul = new Jugador("Raul", 2);

			jdao.saveOrUpdate(adrian);
			jdao.saveOrUpdate(amina);
			jdao.saveOrUpdate(raul);
			
			//creem el continent africa i les seves regions
			Continent africa = new Continent("Africa");
			cdao.saveOrUpdate(africa);
			Regio marruecos = new Regio("Marruecos", africa, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(marruecos);
			Regio congo = new Regio("Congo", africa, raul);
			raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(congo);
			Regio algeria = new Regio("Algeria", africa, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(algeria);
			Regio mali = new Regio("Mali", africa, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(mali);
			Regio libia = new Regio("Libia", africa, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(libia);
			rrr = rdao.findAll();
			for(Regio r : rrr) {
				if(r.getContinent().equals(africa)) {
					africa.getRegions().add(r);
				}
			}
			for (Regio r : africa.getRegions()) {
				r = setVeins(africa, r);
			}
			continents.add(africa);

			//creem el continent africa
			Continent europa = new Continent("Europa");
			cdao.saveOrUpdate(europa);
			Regio espanya = new Regio("Espanya", europa, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(espanya);
			Regio francia = new Regio("Francia", europa, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(francia);
			Regio alemania = new Regio("Alemania", europa, raul);
			raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(alemania);
			Regio italia = new Regio("Italia", europa, raul);
			raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(italia);
			Regio austria = new Regio("Austria", europa, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(austria);
			rrr = rdao.findAll();
			for(Regio r : rrr) {
				if(r.getContinent().equals(europa)) {
					europa.getRegions().add(r);
				}
			}
			for (Regio r : europa.getRegions()) {
				r = setVeins(europa, r);
			}
			continents.add(europa);
			
			//creem el continent asia
			Continent asia = new Continent("Asia");
			cdao.saveOrUpdate(asia);
			Regio afganistan = new Regio("Afganistan", asia, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(afganistan);
			Regio bangladesh = new Regio("Bangladesh", asia, raul);
			raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(bangladesh);
			Regio butan = new Regio("Butan", asia, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(butan);
			Regio camboya = new Regio("Camboya", asia, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(camboya);
			Regio china = new Regio("China", asia, raul);
			raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(china);
			
			rrr = rdao.findAll();
			for(Regio r : rrr) {
				if(r.getContinent().equals(asia)) {
					asia.getRegions().add(r);
				}
			}
			
			for (Regio r : asia.getRegions()) {
				r = setVeins(asia, r);
			}
			continents.add(asia);
			
			//asia relacio amb europa i africa
			china.getVeins().add(marruecos);
			afganistan.getVeins().add(alemania);
			espanya.getVeins().add(marruecos);
			marruecos.getVeins().add(alemania);
			austria.getVeins().add(congo);
			
			rdao.saveOrUpdate(china);
			rdao.saveOrUpdate(afganistan);
			rdao.saveOrUpdate(austria);
			rdao.saveOrUpdate(alemania);
			rdao.saveOrUpdate(congo);
			rdao.saveOrUpdate(espanya);
			rdao.saveOrUpdate(marruecos);		
			jdao.saveOrUpdate(amina);
			jdao.saveOrUpdate(adrian);
			jdao.saveOrUpdate(raul);
		} else if(numJugadors==4) {

			Jugador adrian = new Jugador("Adrian", 0);
			Jugador amina = new Jugador("Amina", 1);
			Jugador raul = new Jugador("Raul", 2);
			Jugador eloi = new Jugador("Eloi", 3);
			
			jdao.saveOrUpdate(adrian);
			jdao.saveOrUpdate(amina);
			jdao.saveOrUpdate(raul);
			jdao.saveOrUpdate(eloi);
			
			//creem el continent africa i les seves regions
			Continent africa = new Continent("Africa");
			cdao.saveOrUpdate(africa);
			Regio marruecos = new Regio("Marruecos", africa, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(marruecos);
			Regio congo = new Regio("Congo", africa, raul);
			raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(congo);
			Regio algeria = new Regio("Algeria", africa, eloi);
			eloi.setQuantitatRegions(eloi.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(algeria);
			Regio mali = new Regio("Mali", africa, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(mali);
			Regio libia = new Regio("Libia", africa, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(libia);
			rrr = rdao.findAll();
			for(Regio r : rrr) {
				if(r.getContinent().equals(africa)) {
					africa.getRegions().add(r);
				}
			}
			for (Regio r : africa.getRegions()) {
				r = setVeins(africa, r);
			}
			continents.add(africa);

			//creem el continent europa i regions
			Continent europa = new Continent("Europa");
			cdao.saveOrUpdate(europa);
			Regio espanya = new Regio("Espanya", europa, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(espanya);
			Regio francia = new Regio("Francia", europa, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(francia);
			Regio alemania = new Regio("Alemania", europa, eloi);
			eloi.setQuantitatRegions(eloi.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(alemania);
			Regio italia = new Regio("Italia", europa, raul);
			raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(italia);
			Regio austria = new Regio("Austria", europa, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(austria);
			rrr = rdao.findAll();
			for(Regio r : rrr) {
				if(r.getContinent().equals(europa)) {
					europa.getRegions().add(r);
				}
			}
			for (Regio r : europa.getRegions()) {
				r = setVeins(europa, r);
			}
			continents.add(europa);
			
			//creem el continent asia
			Continent asia = new Continent("Asia");
			cdao.saveOrUpdate(asia);
			Regio afganistan = new Regio("Afganistan", asia, eloi);
			eloi.setQuantitatRegions(eloi.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(afganistan);
			Regio bangladesh = new Regio("Bangladesh", asia, raul);
			raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(bangladesh);
			Regio butan = new Regio("Butan", asia, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(butan);
			Regio camboya = new Regio("Camboya", asia, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(camboya);
			Regio china = new Regio("China", asia, raul);
			raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(china);
			
			rrr = rdao.findAll();
			for(Regio r : rrr) {
				if(r.getContinent().equals(asia)) {
					asia.getRegions().add(r);
				}
			}
			
			for (Regio r : asia.getRegions()) {
				r = setVeins(asia, r);
			}
			continents.add(asia);
			
			//creem el continent america i les seves regions
			Continent america = new Continent("America");
			cdao.saveOrUpdate(america);
			Regio argentina = new Regio("Argentina", america, adrian);
			adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(argentina);
			Regio canada = new Regio("Canada", america, amina);
			amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(canada);
			Regio colombia = new Regio("Colombia", america, eloi);
			eloi.setQuantitatRegions(eloi.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(colombia);
			Regio USA = new Regio("USA", america, eloi);
			eloi.setQuantitatRegions(eloi.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(USA);
			Regio venezuela = new Regio("Venezuela", america, raul);
			raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
			rdao.saveOrUpdate(venezuela);
			rrr = rdao.findAll();
			for(Regio r : rrr) {
				if(r.getContinent().equals(america)) {
					america.getRegions().add(r);
				}
			}
			for (Regio r : america.getRegions()) {
				r = setVeins(america, r);
			}
			continents.add(america);
			
			//relacio america amb africa asia europa
			argentina.getVeins().add(italia);
			canada.getVeins().add(francia);
			
			USA.getVeins().add(china);
			venezuela.getVeins().add(camboya);
			
			colombia.getVeins().add(libia);
			
			//asia relacio amb europa i africa
			china.getVeins().add(marruecos);
			afganistan.getVeins().add(alemania);
			espanya.getVeins().add(marruecos);
			marruecos.getVeins().add(alemania);
			austria.getVeins().add(congo);
			argentina.getVeins().add(china);
			bangladesh.getVeins().add(colombia);
			italia.getVeins().add(butan);
			espanya.getVeins().add(afganistan);
			italia.getVeins().add(espanya);
			venezuela.getVeins().add(canada);
			
			rdao.saveOrUpdate(butan);
			rdao.saveOrUpdate(bangladesh);
			rdao.saveOrUpdate(USA);
			rdao.saveOrUpdate(argentina);
			rdao.saveOrUpdate(canada);
			rdao.saveOrUpdate(italia);
			rdao.saveOrUpdate(francia);
			rdao.saveOrUpdate(venezuela);
			rdao.saveOrUpdate(camboya);
			rdao.saveOrUpdate(colombia);
			rdao.saveOrUpdate(libia);
			rdao.saveOrUpdate(china);
			rdao.saveOrUpdate(afganistan);
			rdao.saveOrUpdate(austria);
			rdao.saveOrUpdate(alemania);
			rdao.saveOrUpdate(congo);
			rdao.saveOrUpdate(espanya);
			rdao.saveOrUpdate(marruecos);		
			jdao.saveOrUpdate(amina);
			jdao.saveOrUpdate(adrian);
			jdao.saveOrUpdate(raul);
			jdao.saveOrUpdate(eloi);
		}
		
		for (Continent c : continents) {
			saveContinentRegions(cdao, rdao, c);
		}
		
		bbddCargada = true;
	}

	private static void saveContinentRegions(IContinentDAO cdao, IRegioDAO rdao, Continent c) {
		cdao.saveOrUpdate(c);
		for (Regio r : c.getRegions()) {
			rdao.saveOrUpdate(r);
		}
	}

	private static Regio setVeins(Continent c, Regio r) {
		for (Regio rr : c.getRegions()) {
			if (!rr.equals(r)) {
				if(!rr.getVeins().contains(r)) {
					r.getVeins().add(rr);
				}
			}
		}
		return r;
	}

}
