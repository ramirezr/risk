/**
 * 
 */
package Main;

import java.util.Scanner;

/**
 * @author IES SABADELL
 *
 */
public class LeerScanner {

private Scanner reader;
	
	public LeerScanner(){
		// Important, llegeixo de l'entrada de teclat.
		reader = new Scanner(System.in);
	}
	// M�tode que llegeix una linia de la consola
	public String LlegirPantalla() {
		String retorn = "";
		retorn = reader.nextLine();
		return retorn;
	}
	// M�tode per tancar el flux de dades
	public void TancarReader() {
		reader.close();
	}

}
