package DAO;

import java.io.Serializable;
import java.util.List;

import Model.Continent;

public interface IContinentDAO extends IGenericDAO<Continent, Integer>, Serializable {

	/**
	 * Es comprova mitjan�ant consulta a la BD si algun jugador ha obtingut un
	 * continent o b� l�ha perdut a favor d�un altre jugador o simplement l�ha
	 * perdut. El resultat es mostra en un text per pantalla
	 */
	public void CanviarPropietariContinent();
	
	public void LlistarContinentsJugador(int CodiJugador);

}
