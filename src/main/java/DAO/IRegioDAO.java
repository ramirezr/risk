package DAO;

import java.io.Serializable;
import java.util.List;

import Model.Regio;

public interface IRegioDAO extends IGenericDAO<Regio, Integer>, Serializable {

	/**
	 * A partir de les tropes que li pertoquen al jugador, aquest les distribueix de
	 * manera aleat�ria a les diferents regions. Aquests canvis, s�actualitzen a la
	 * BD i es mostra el resultat en format text.
	 * 
	 * @param CodiUsuari
	 * @param Tropes
	 */
	public void PosarTropes(int CodiUsuari, int Tropes);

	/**
	 * Es consulta a la BD quines regions pot atacar el jugador i es mostra el
	 * resultat per pantalla en format JSON.
	 * 
	 * @param CodiUsuari
	 * @return 
	 */
	public List<Regio> MostrarRegionsAtacar(int CodiUsuari);

	/**
	 * S’inicia la fase d’atac amb les regions escollides. Es comprova amb consulta
	 * de BD que hi hagi suficients tropes per poder atacar abans de fer la tirada
	 * de daus. Després del resultat de la tirada de daus, es comprova si alguna
	 * regió és conquerida per l’atacant o defensor. En cas afirmatiu, es canvia de
	 * propietari. Aquests canvis de propietaris i de número de tropes s’han
	 * d’actualitzar a la BD.
	 * 
	 * @param RegioAtacant
	 * @param RegioAtacada
	 * @param tropesPerdudesAtacant
	 * @param tropesPerdudesDefensor
	 */
	public void Atac(Regio RegioAtacant, Regio RegioAtacada);

	/**
	 * Abans d�acabar el torn, un jugador pot moure tropes. Aquesta decisi�, hauria
	 * de ser aleat�ria o b� tenir un algoritme que prengui la decisi�. Recorda que
	 * cap regi� es pot quedar sense cap tropa. Es mou unes tropes d�una regi� a una
	 * altra. El resultat del moviment es mostra en un text per pantalla.
	 */
	public void FaseReestructuracio(int CodiJugador);

	/**
	 * Es tiren els daus com s�indiquen a la fase del joc i es resolt qui perd
	 * tropes. El resultat es mostra en un text per pantalla.
	 * 
	 * @param TropesAtacant
	 * @param TropesAtacada
	 */
	public List<Integer> TirarDaus(int TropesAtacant, int TropesAtacada);

	/**
	 * Decideixes si vols atacar o no. En cas afirmatiu, mostres les regions que el
	 * jugador pot atacar, esculls les regions per fer l’atac. Després d’atacar,
	 * mostres el resultat de l’atac. Un jugador pot estar a la fase d’atac fins que
	 * prengui la decisió de no atacar més.
	 * @return 
	 */
	public void FaseAtac(int CodiUsuari, boolean atacar);
}
