package DAO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import Model.Jugador;
import Model.Regio;

public class RegioDAOImpl extends GenericDAOImpl<Regio, Integer> implements IRegioDAO {

	private Random random = new Random();

	public void PosarTropes(int CodiUsuari, int Tropes) {

		List<Regio> regionsJugador = getRegionsJugador(CodiUsuari);
		if (!regionsJugador.isEmpty()) {
			System.out.println();
			while (Tropes > 0) {
				int tropesAEntregar = random.nextInt(Tropes) + 1;
				int regioRandom = random.nextInt(regionsJugador.size());
				Regio r = regionsJugador.get(regioRandom);
				int tropesActuals = r.getTropes() + tropesAEntregar;
				r.setTropes(tropesActuals);
				saveOrUpdate(r);
				System.out.println("S'ha afegit " + tropesAEntregar + " tropes a " + r.getNom());
				Tropes -= tropesAEntregar;
			}
		}
	}

	public List<Regio> MostrarRegionsAtacar(int codiUsuari) {
		System.out.println();
		List<Regio> regions = this.findAll();
		Set<Regio> regionsAtacar = new HashSet<Regio>();
		for (Regio r : regions) {
			if (r.getPropietariRegio().getId() == codiUsuari) {
				Set<Regio> regionsVeines = r.getVeins();
				for (Regio re : regionsVeines) {
					if (re.getPropietariRegio().getId() != codiUsuari) {
						regionsAtacar.add(re);
					}
				}
			}
		}
		if(!regionsAtacar.isEmpty()) {
			for (Regio reg : regionsAtacar) {
				System.out.println(reg.getNom() + " pot ser atacada");
			}
		} else System.err.println("No hi ha regions a atacar!");
		
		return new ArrayList<Regio>(regionsAtacar);

	}

	public void FaseReestructuracio(int codiJugador) {
		List<Regio> regionsJugador = getRegionsJugador(codiJugador);
		List<Regio> regionsDeMesUnaTropa = new ArrayList<Regio>();

		if (!regionsJugador.isEmpty() && regionsJugador.size() > 1) {

			for (Regio r : regionsJugador) {
				if (r.getTropes() > 1) {
					regionsDeMesUnaTropa.add(r);
				}
			}
			
			if(regionsDeMesUnaTropa.size()==0) {
				System.out.println("No tens tropes suficients per moure!");
			} else {
				int ran1 = random.nextInt(regionsDeMesUnaTropa.size());
				int ran2 = random.nextInt(regionsJugador.size());

				Regio regioMoure = regionsDeMesUnaTropa.get(ran1);
				Regio regioRebre = regionsJugador.get(ran2);

				while (regioMoure.equals(regioRebre)) {
					ran2 = random.nextInt(regionsJugador.size());
					regioRebre = regionsJugador.get(ran2);
				}

				int tropesRegioMoure = regioMoure.getTropes();
				int tropesAPassar = random.nextInt(tropesRegioMoure) + 1;

				while (tropesAPassar == tropesRegioMoure) {
					tropesAPassar = random.nextInt(tropesRegioMoure) + 1;
				}

				regioMoure.setTropes(regioMoure.getTropes() - tropesAPassar);
				regioRebre.setTropes(regioRebre.getTropes() + tropesAPassar);
				saveOrUpdate(regioMoure);
				saveOrUpdate(regioRebre);
				
				System.out.println("El jugador "+ regionsJugador.get(0).getPropietariRegio().getNom() + " vol moure tropes...");
				System.out.println(
						regionsJugador.get(0).getPropietariRegio().getNom()+ " ha mogut " + tropesAPassar + " tropes de " + regioMoure.getNom() + " a " + regioRebre.getNom());
				System.out.println();

			}
		}
	}

	public void Atac(Regio RegioAtacant, Regio RegioAtacada) {
		
		if (RegioAtacant.getTropes() > 1 && RegioAtacada.getPropietariRegio().getId() != RegioAtacant.getPropietariRegio().getId()) {
			Jugador jAtacant = RegioAtacant.getPropietariRegio();
			Jugador jDefendent = RegioAtacada.getPropietariRegio();
			System.out.println();
			System.out.println("Tropes regio "+RegioAtacant.getNom()+" atacant: " + RegioAtacant.getTropes());
			System.out.println("Tropes regio "+RegioAtacada.getNom()+" defensora: " + RegioAtacada.getTropes());
			int atacant;
			int defendent;
			int tropesAtacantAux;
			int tropesDefendentAux;
			int dice;// si mes de 0, l'atacant guanya la tirada, si 0 o menys, la perd.

			// el atacante solo puede atacar con max 3 tropas y siempre tiene que dejar una
			// atras en su region.
			// el defensor solo puede defender con un max de 2 tropas.
			// se tiraran los dados mientras que las tropas del atacante o del defensor sean
			// > 0
			System.out.println(RegioAtacant.getNom() + " va a atacar a " + RegioAtacada.getNom());
			if (RegioAtacada.getTropes() > 2) {
				defendent = 2;
			} else {
				defendent = RegioAtacada.getTropes();
			}
			if (RegioAtacant.getTropes() > 1 && RegioAtacant.getTropes() < 4) {
				atacant = RegioAtacant.getTropes() - 1;
			} else { // RegioAtacant.getTropes()==1
				atacant = 3;
			}
			System.out.println("El jugador "+RegioAtacant.getPropietariRegio().getNom()+" ataca amb la regio "+RegioAtacant.getNom()+""
					+ " al jugador "+RegioAtacada.getPropietariRegio().getNom()+" regio "+RegioAtacada.getNom());
			System.out.println("Tropes de la regio atacant "+RegioAtacant.getNom()+": " + atacant);
			System.out.println("Tropes de la regio defensora "+RegioAtacada.getNom()+": " + defendent);
			tropesAtacantAux = atacant;
			tropesDefendentAux = defendent;

			// com s'indica a les regles del joc,
			// https://www.ultraboardgames.com/risk/game-rules.php
			// es tiren el mateix numero de daus que tropes s'utilitzen.
			// si un jugador te 3 tropes atacant, es tiren 3 daus, i el defensor si te 2
			// tira 2 daus
			// s'agafen el valors dels daus mes grans i es comparen.
			List<Integer> resultAtac = TirarDaus(atacant, defendent);
			atacant = resultAtac.get(0);
			System.out.println("La regio atacant "+RegioAtacant.getNom()+" ha perdut: " + (tropesAtacantAux-atacant));
			defendent = resultAtac.get(1);
			System.out.println("La regio defensora "+RegioAtacada.getNom()+" ha perdut: " + (tropesDefendentAux-defendent));
			
			if (defendent > 0) {
				RegioAtacant.setTropes(RegioAtacant.getTropes() - (tropesAtacantAux - atacant));
				RegioAtacada.setTropes(RegioAtacada.getTropes() - (tropesDefendentAux - defendent));
				System.out.println(RegioAtacada.getPropietariRegio().getNom()+" guanya la defensa de "+ RegioAtacada.getNom());
			} else if (defendent == 0) {
				System.out.println(RegioAtacant.getPropietariRegio().getNom()+" guanya atac amb"+ RegioAtacant.getNom() + " contra la regio "+RegioAtacada.getNom());
				RegioAtacada.setTropes(0);
				RegioAtacada.setPropietariRegio(RegioAtacant.getPropietariRegio());
				System.out.println("La regio "+RegioAtacada.getNom() + " ara te com a propietari "+RegioAtacada.getPropietariRegio().getNom());
				RegioAtacant.setTropes(RegioAtacant.getTropes() - (tropesAtacantAux - atacant));
				RegioAtacada.setTropes(RegioAtacant.getTropes() - 1);
				RegioAtacant.setTropes(1);
				jAtacant.setQuantitatRegions(jAtacant.getQuantitatRegions()+1);
				jDefendent.setQuantitatRegions(jDefendent.getQuantitatRegions()-1);
				System.out.println("La regio " + RegioAtacada.getNom() + " te " +RegioAtacada.getTropes()+ " tropes.");
				System.out.println("La regio " + RegioAtacant.getNom() + " te " + RegioAtacant.getTropes() + " tropes.");
			}
			System.out.println();
			this.saveOrUpdateJ(jDefendent);
			this.saveOrUpdateJ(jAtacant);
			this.saveOrUpdate(RegioAtacada);
			this.saveOrUpdate(RegioAtacant);
		}
	}

	public List<Integer> TirarDaus(int TropesAtacant, int TropesAtacada) {
		List<Integer> atacant = new ArrayList<Integer>();
		List<Integer> defendent = new ArrayList<Integer>();
		List<Integer> result = new ArrayList<Integer>();
		int diceCompare;

		for (int i = 0; i < TropesAtacant; i++) { // afegim daus depenent del nombre de tropes que l'atacant té.
			atacant.add(random.nextInt(6) + 1);
		}
		Collections.sort(atacant, Collections.reverseOrder()); // les ordenem de mes gran a mes petit per comparar mes
		System.out.println("Resultat dels daus de latacant: "+atacant);				// tard.

		for (int i = 0; i < TropesAtacada; i++) { // afegim daus depenent del nombre de tropes que el defensor té.
			defendent.add(random.nextInt(6) + 1);
		}
		Collections.sort(defendent, Collections.reverseOrder()); // les ordenem de mes gran a mes petit per comparar mes
		System.out.println("Resultat dels daus del defensor: "+defendent);				// tard.

		// cuants daus hem de comparar?
		diceCompare = Math.min(atacant.size(), defendent.size());

		// si dau atacant es mes gran que el dau del defensor, l'atacant mata una tropa,
		// si el del defensor es igual o major al de l'atacant, mata una tropa
		for (int i = 0; i < diceCompare; i++) {
			if (atacant.get(i) > defendent.get(i)) {
				TropesAtacada--;
			} else {
				TropesAtacant--;
			}
		}
		result.add(TropesAtacant);
		result.add(TropesAtacada);
		return result;
	}

	public List<Regio> getRegionsJugador(int codiJugador) {
		List<Regio> regions = this.findAll();
		List<Regio> regionsJugador = new ArrayList<Regio>();
		if (regions != null) {
			for (Regio r : regions) {
				if (r.getPropietariRegio().getId() == codiJugador) {
					regionsJugador.add(r);
				}
			}
		}
		return regionsJugador;
	}

	public void FaseAtac(int CodiUsuari, boolean atacar) {
		if (atacar) {
			List<Regio> regionsAtacables = MostrarRegionsAtacar(CodiUsuari);
			if (regionsAtacables.size() > 0) {
				List<Regio> regionsJugador = getRegionsJugador(CodiUsuari);
				
				for(Regio rAtacable: regionsAtacables) {
					for(Regio rAtacant : regionsJugador) {
						if(rAtacant.getVeins().contains(rAtacable)) {
							Atac(rAtacant, rAtacable);
						}
					}
				}
			}

		} else {
			System.out.println("El jugador no vol atacar");
		}
	}

}
