package DAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import Model.Continent;
import Model.Jugador;
import Model.Regio;

public class ContinentDAOImpl extends GenericDAOImpl<Continent, Integer> implements IContinentDAO {

	public void CanviarPropietariContinent() {
		List<Continent> continents;
		Jugador j = null;
		int cont = 0;
		continents = this.findAll();
		for (Continent c : continents) {
			cont = 0;
			if (c.getRegions() != null) {
				for (Regio r : c.getRegions()) {
					if (r.getPropietariRegio() != null && j == null) {
						j = r.getPropietariRegio();
					}
					if ((j != null && r.getPropietariRegio() != null) && j.equals(r.getPropietariRegio())) {
						cont++;
					}
				}
				if (cont == c.getRegions().size()) {
					if (c.getPropietari() != null && c.getPropietari().equals(j)) {
						System.out.println(c.getPropietari().getNom() + " es el propietari de " + c.getNom());
					} else {
						c.setPropietari(j);
						this.saveOrUpdate(c);
						System.out
								.println(c.getPropietari().getNom() + " ha ganado un nuevo continente: " + c.getNom());
					}
				} else {
					if (c.getPropietari() != null) {
						System.err.println(c.getPropietari().getNom() + " ha perdut " + c.getNom());
						c.setPropietari(null);
						this.saveOrUpdate(c);
					} else {
						System.out.println(c.getNom() + " no tiene propietario");
					}
				}
				j = null;
			}
		}

	}

	public void LlistarContinentsJugador(int CodiJugador) {
		List<Continent> continents = this.findAll();

		for (Continent c : continents) {
			if (c.getPropietari() != null && c.getPropietari().getId() == CodiJugador) {
				System.out.println(c.getNom());
			}
		}

	}
}
