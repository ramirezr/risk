package DAO;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import Model.Continent;
import Model.Jugador;
import Model.Regio;

public class JugadorDAOImpl extends GenericDAOImpl<Jugador, Integer> implements IJugadorDAO {


	private static Set<String> continents = new HashSet<String>();
	
	public void ComptarContinents(int CodiJugador) {
		Jugador j = get(CodiJugador);
		System.out.println("Jugador "+j.getNom()+" te " +j.getContinents().size()+ " continents.");
	}
	
	public int CalcularTropes(int CodiJugador) {
		Jugador j = get(CodiJugador);
		List<Regio> regions = new ArrayList<Regio>(j.getRegions());
		List<Continent> conts = new ArrayList<Continent>(j.getContinents());
		
		//el asignar un numero tan elevado de tropas hacia que la partida se alargase mucho.
		//hacemos el mismo metodo pero en vez de 5 asignamos 1 para comprobar que la simulacion acaba.
		//int quantitatTropes = 5 + (regions.size()/3) + conts.size();
		int quantitatTropes = 0;
		if(!regions.isEmpty()) {
			System.out.println();
			quantitatTropes = 5 + (regions.size()/3) + conts.size();
			System.out.println("Jugador "+j.getNom()+" rep "+ quantitatTropes +" tropes.");
		} else if(regions.isEmpty()) {
			System.err.println("El jugador " +j.getNom()+" ja esta eliminat!");
		}
		return quantitatTropes;

		
	}
	

	public boolean FinalitzarPartida() {
		List<Jugador> jugadors = findAll();
		if(continents.isEmpty()) {
			getContinents();
		}
		for(Jugador j : jugadors) {
			if(j.getContinents().size()==continents.size()) {
				System.out.println();
				j.setComptadorVictories(j.getComptadorVictories()+1);
				this.saveOrUpdate(j);
				System.out.println("Ja tenim un guanyador! Enhorabona "+j.getNom()+"!");
				
				return true;
			}
		}
		return false;
	}
	
	private Set<String> getContinents(){
		List<Jugador> jugadors = this.findAll();
		for(Jugador j : jugadors) {
			for(Regio r : j.getRegions()) {
				continents.add(r.getContinent().getNom());
			}
		}
		return continents;
	}

}