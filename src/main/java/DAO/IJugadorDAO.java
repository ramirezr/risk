package DAO;

import java.io.Serializable;

import Model.Jugador;

public interface IJugadorDAO extends IGenericDAO<Jugador, Integer>, Serializable {

	/**
	 * Compta quants continents té un usuari consultant a la BD.
	 * 
	 * @param CodiJugador
	 */
	public void ComptarContinents(int CodiJugador);

	/**
	 * Calcula les tropes que rebra al començament del torn el jugador.
	 * 
	 * @param CodiJugador
	 */
	public int CalcularTropes(int CodiJugador);

	/**
	 * Es comprova mitjan�ant una consulta a la BD si un jugador ja no t� m�s
	 * regions i per tant ha perdut. O b�, si un jugador �s guanyador perqu� t�
	 * totes les regions. Es mostra en un text per pantalla.
	 */
	public boolean FinalitzarPartida();

}
