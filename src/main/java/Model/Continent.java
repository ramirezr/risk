package Model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="Continent")
public class Continent implements Serializable {

	@Id
	@Column(name="IdContinent")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="Nom",nullable=false,length=25)
	private String nom;
	/*@Column(name="Bonus",columnDefinition="boolean default false",nullable=false)
	private boolean bonus;
	*/
	@Column(name="Bonus", nullable=false)
	private int bonus;
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "continent",fetch = FetchType.EAGER)
	private Set<Regio> regions;

	@ManyToOne()
	@JoinColumn(name="PropietariID",nullable=true)
	private Jugador propietariContinent;
	
	
	
	public Continent() {
		super();
		regions = new HashSet<Regio>();
		this.bonus=1;
	}

	public Continent(String nom) {
		super();
		this.nom = nom;
		regions = new HashSet<Regio>();
		this.bonus=1;

	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Set<Regio> getRegions() {
		return regions;
	}

	public void setRegions(Set<Regio> regions) {
		this.regions = regions;
	}

	public Jugador getPropietari() {
		return propietariContinent;
	}

	public void setPropietari(Jugador propietari) {
		this.propietariContinent = propietari;
	}
	
	/*public boolean hasBonus() {
		return bonus;
	}

	public void setBonus(boolean bonus) {
		this.bonus = bonus;
	}*/

	public int getBonus() {
		return bonus;
	}

	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Continent other = (Continent) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Continent [id=" + id + ", nom=" + nom + ", bonus=" + bonus + "propietariContinent=" + propietariContinent + "]";
	}
	
}
