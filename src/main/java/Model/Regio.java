package Model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Regio")
public class Regio implements Serializable {

	@Id
	@Column(name = "IdRegio")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "Nom", nullable = false, length = 25)
	private String nom;
	/**
	 * La region tiene las tropas, y el jugador tiene la region De esa manera
	 * podemos saber el numero de tropas que tiene un jugador.
	 */
	@Column(name = "Tropes", nullable = false)
	private int tropes;

	@ManyToOne
	@JoinColumn(name = "ContinentID")
	private Continent continent;

	@ManyToOne
	@JoinColumn(name = "PropietariID")
	private Jugador propietariRegio;

	// TODO en principio así ya está bien. No hacen falta 2 metodos Set, con uno es
	// suficiente, porque tampoco queremos que se repita la información? con
	// fetchtype funciona correctamente en principio
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "RegioVeins", joinColumns = @JoinColumn(name = "vei"), inverseJoinColumns = @JoinColumn(name = "veiDe"))
	private Set<Regio> veins;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "RegioVeins", joinColumns = @JoinColumn(name = "veiDe"), inverseJoinColumns = @JoinColumn(name = "vei"))
	private Set<Regio> veinaDe;

	public Regio() {
		super();
		veins = new HashSet<Regio>();
		veinaDe = new HashSet<Regio>();
	}

	/**
	 * continente deberia ir en el constructor? REVISAR
	 * 
	 * @param nom
	 * @param tropes
	 * @param continent
	 */
	public Regio(String nom, Continent continent) {
		super();
		this.nom = nom;
		this.tropes = 5;
		this.continent = continent;
		veins = new HashSet<Regio>();
		veinaDe = new HashSet<Regio>();
	}

	public Regio(String nom, Continent continent, Jugador propietariRegio) {
		super();
		this.nom = nom;
		this.continent = continent;
		this.propietariRegio = propietariRegio;
		this.tropes = 5;
		veins = new HashSet<Regio>();
		veinaDe = new HashSet<Regio>();
	}

	public int getIdRegio() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getTropes() {
		return tropes;
	}

	public void setTropes(int tropes) {
		this.tropes = tropes;
	}

	public Continent getContinent() {
		return continent;
	}

	public void setContinent(Continent continent) {
		this.continent = continent;
	}

	public Set<Regio> getVeins() {
		return veins;
	}

	public void setVeins(Set<Regio> veins) {
		this.veins = veins;
	}

	public Jugador getPropietariRegio() {
		return propietariRegio;
	}

	public void setPropietariRegio(Jugador propietariRegio) {
		this.propietariRegio = propietariRegio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Regio other = (Regio) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "\nRegio [nom=" + nom + ", tropes=" + tropes + ", continent=" + continent + ", propietariRegio="
				+ propietariRegio + "]";
	}

}
